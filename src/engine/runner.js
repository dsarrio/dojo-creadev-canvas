
class Runner {

    constructor(canvasId, rendererClass) {
        this.bindCanvas(canvasId);

        this.renderer = new rendererClass({
            canvas: this.canvas,
            ctx: this.ctx,
            width: this.canvas.width,
            height: this.canvas.height,
        });

        this.frameDelay = 1000 / 60;
        document.getElementById('fps').addEventListener('input', (e) => {
            this.frameDelay = 1000 / e.target.value;
            this.prevFrame = 0;
        });

        this.timeDisplay = document.getElementById('time');
        this.startTime = 0;
        requestAnimationFrame((time) => {
            this.startTime = time;
            this.prevFrame = 0;
            this.redraw(time)
        });

        this.stats = { begin: () => {}, end: () => {} }
        if (typeof Stats !== 'undefined') {
            this.stats = new Stats();
            this.stats.showPanel(0);
            const elt = this.stats.dom;
            elt.style.cssText = 'cursor:pointer; z-index:10000';
            document.getElementById('stats_container').appendChild(elt);
        }
    }

    bindCanvas(canvasId) {
        console.log('binding to canvas: ' + canvasId);
        this.canvas = document.getElementById(canvasId);
        this.canvas.addEventListener('mousemove', (e) => this.onMouseMoved(e));
        this.canvas.addEventListener('click', (e) => this.onMouseClicked(e));
        this.mouse = { x: this.canvas.width / 2, y: this.canvas.height / 2 };
        this.ctx = this.canvas.getContext("2d");
        document.addEventListener('keydown', (e) => this.onKeyDown(e));
    }

    redraw(currentTime) {
        const frame = Math.trunc((currentTime - this.startTime) / this.frameDelay);
        const elapsedTimeInSec = (currentTime - this.startTime) / 1000;
        const rounded = Math.round(elapsedTimeInSec * 10);
        this.timeDisplay.innerHTML = '' + rounded/10 + ((rounded % 10 == 0) ? '.0s' : 's');
        if (frame > this.prevFrame) {
            this.prevFrame = frame;
            this.stats.begin();
            this.renderer.draw({
                ctx: this.ctx,
                time: elapsedTimeInSec,
            });
            this.stats.end();
        }

        requestAnimationFrame((t) => this.redraw(t));
    }

    onKeyDown({code}) {
        if (code === 'KeyR') {
            requestAnimationFrame((t) => this.redraw(t));
        }
    }

    onMouseMoved({clientX, clientY}) {
        if (this.renderer.onMouseMoved) {      
            const canvas_rect = this.canvas.getBoundingClientRect();      
            this.renderer.onMouseMoved({
                mouse: {
                    x: clientX - canvas_rect.left,
                    y: clientY - canvas_rect.top,
                }
            })
        }
    }

    onMouseClicked({clientX, clientY}) {
        if (this.renderer.onMouseClicked) {
            const canvas_rect = this.canvas.getBoundingClientRect();      
            this.renderer.onMouseClicked({
                mouse: {
                    x: clientX - canvas_rect.left,
                    y: clientY - canvas_rect.top,
                }
            });
        }
    }
}
