
function clamp(v, min, max) {
    return  v < min ? min : v > max ? max : v;
}

function lerp(start, end, factor) {
    return start + factor * (end - start);
}

function easeinout(x) {
    return x < 0.5 ? 16 * x * x * x * x * x : 1 - Math.pow(-2 * x + 2, 5) / 2;
}

function easein(x) {
    return Math.pow(x, 3);
}

function easeout(x) {
    return 1 - Math.pow(1 - x, 3);
}

function rgb(r, g, b) {
    const p = [r, g, b].map((v) => {
        v = Math.round(clamp(v, 0, 1) * 255.0);
        return ((v < 16) ? "0" : "") + v.toString(16);
    });
    return "#" + p.join("").toUpperCase();
}
