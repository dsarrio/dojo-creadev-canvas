
/*

const url_params = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop) => searchParams.get(prop),
    set: (searchParams, prop, value) => {
        searchParams.set(prop, value);
        window.history.pushState({}, '', "/?" + searchParams.toString());
        return true;
    },
});

*/

class HelpersController {

    color = {
        hex: '#ffffff',
        r: 1,
        g: 1,
        b: 1,
    };

    scale = {
        value: 30,
        factor: 0.3,
        min: 0,
        max: 100,
    }

    toggle = true;

    constructor() {
        // color picker
        this.colorDisplayHex = document.getElementById('color_display_hex');
        this.colorDisplayR = document.getElementById('color_display_r');
        this.colorDisplayG = document.getElementById('color_display_g');
        this.colorDisplayB = document.getElementById('color_display_b');
        this.colorPicker = document.getElementById('color');
        this.colorPicker.addEventListener('input', (e) => this.onColorChanged(e));

        // scale min
        this.scaleDisplayMin = document.getElementById('scale_display_min');
        this.scalePicker_min = document.getElementById('scale_min');
        this.scalePicker_min.addEventListener('input', (e) => this.onScaleChanged(e));

        // scale max
        this.scaleDisplayMax = document.getElementById('scale_display_max');
        this.scalePicker_max = document.getElementById('scale_max');
        this.scalePicker_max.addEventListener('input', (e) => this.onScaleChanged(e));

        // scale slider
        this.scaleDisplayValue = document.getElementById('scale_display_value');
        this.scaleDisplayFactor = document.getElementById('scale_display_factor');
        this.scalePicker = document.getElementById('scale');
        this.scalePicker.addEventListener('input', (e) => this.onScaleChanged(e));

        // toggles
        this.toggleDisplay = document.getElementById('toggle_display');
        this.togglePicker = document.getElementById('toggle');
        this.togglePicker.addEventListener('input', (e) => this.onToggleChanged(e));

        //
        this.init();
    }

    init() {
        const str = localStorage.getItem('helpers');
        if (str) {
            const data = JSON.parse(str);
            this.color = data.color;
            this.scale = data.scale;
            this.toggle = data.toggle;
        }

        this.colorPicker.value = this.color.hex;
        this.scalePicker.value = this.scale.value / (this.scale.max - this.scale.min) * 1000;
        this.scalePicker_min.value = this.scale.min;
        this.scalePicker_max.value = this.scale.max;
        this.togglePicker.checked = this.toggle;

        this.refreshColorDisplays();
        this.refreshScaleDisplays();
        this.refreshToggleDisplays();
    }

    save() {
        const data = { color: this.color, scale: this.scale, toggle: this.toggle };
        localStorage.setItem('helpers', JSON.stringify(data));
    }

    onColorChanged({target}) {
        this.color = {
            hex: target.value,
            r: parseInt(target.value.substr(1, 2), 16),
            g: parseInt(target.value.substr(3, 2), 16),
            b: parseInt(target.value.substr(5, 2), 16),
        };
        this.refreshColorDisplays();
        this.save();
    }

    refreshColorDisplays() {
        this.colorDisplayHex.innerText = this.color.hex;
        this.colorDisplayR.innerText = this.color.r;
        this.colorDisplayG.innerText = this.color.g;
        this.colorDisplayB.innerText = this.color.b;
    }

    onScaleChanged({target}) {
        this.scale.min = parseInt(this.scalePicker_min.value);
        this.scale.max = parseInt(this.scalePicker_max.value);
        this.scale.factor = target.value / 1000;
        this.scale.value = lerp(this.scale.min, this.scale.max, this.scale.factor);
        this.refreshScaleDisplays();
        this.save();
    }

    refreshScaleDisplays() {
        this.scaleDisplayMin.innerText = this.scale.min;
        this.scaleDisplayMax.innerText = this.scale.max;
        this.scaleDisplayValue.innerText = this.scale.value;
        this.scaleDisplayFactor.innerText = this.scale.factor;
    }

    onToggleChanged({target}) {
        this.toggle = target.checked;
        this.refreshToggleDisplays();
        this.save();
    }

    refreshToggleDisplays() {
        this.toggleDisplay.innerText = this.toggle;
    }
}
